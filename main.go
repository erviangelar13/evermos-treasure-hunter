package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

var x = 8
var y = 6

// var paths = map[string][2]float64{
// 	"x": { 1,
// }
// var maps = map[int][string]float64{}
// 	"N": {0, 1}, "NE": {diagonal, diagonal},
// 	"E": {1, 0}, "SE": {diagonal, -diagonal},
// 	"S": {0, -1}, "SW": {-diagonal, -diagonal},
// 	"W": {-1, 0}, "NW": {-diagonal, diagonal},
// }
var path = [][2]uint8{
	{0, 0},
	{0, 1},
	{0, 2},
	{0, 3},
	{0, 4},
	{0, 5},
	{1, 0},
	{1, 5},
	{2, 0},
	{2, 1},
	{2, 3},
	{2, 5},
	{3, 0},
	{3, 3},
	{3, 5},
	{4, 0},
	{4, 2},
	{4, 3},
	{4, 5},
	{5, 0},
	{5, 5},
	{6, 0},
	{6, 2},
	{6, 5},
	{7, 0},
	{7, 1},
	{7, 2},
	{7, 3},
	{7, 4},
	{7, 5},
}
var current = [][2]uint8{
	{1, 1},
}

var maps [6][8]string

func findPath(item [2]uint8) bool {
	for _, v := range path {
		if item[0] == v[0] && item[1] == v[1] {
			return true
		}
	}
	return false
}

func findCurrent(item [2]uint8) bool {
	for _, v := range current {
		if item[0] == v[0] && item[1] == v[1] {
			return true
		}
	}
	return false
}

func drawMaps() {
	maps = [6][8]string{}
	a := make([][]uint8, x)
	for i := range a {
		a[i] = make([]uint8, y)
		for j := range a[i] {
			var item = [2]uint8{uint8(i), uint8(j)}
			if findPath(item) {
				// maps[j][i] = fmt.Sprintf("#(%d,%d)", j, i)
				maps[j][i] = fmt.Sprintf("#")
			} else {
				// maps[j][i] = fmt.Sprintf(".(%d,%d)", j, i)
				maps[j][i] = fmt.Sprintf(".")
			}

			if findCurrent(item) {
				// maps[j][i] = fmt.Sprintf("X(%d,%d)", j, i)
				maps[j][i] = fmt.Sprintf("X")
			}
		}
	}
	for i := range maps {
		for j, v := range maps[len(maps)-i-1] {
			if j != len(maps[len(maps)-i-1])-1 {
				fmt.Printf(v)
			} else {
				fmt.Println(v)
			}
		}
	}
}

func findProbabilityTreasure() {
	for i := range maps {
		for j, v := range maps[len(maps)-i-1] {
			if v == "." {

			}
			if j != len(maps[len(maps)-i-1])-1 {
				fmt.Printf(v)
			} else {
				fmt.Println(v)
			}
		}
	}
}

func setCurrent(direction string, step int) {
	var x = current[0][0]
	var y = current[0][1]
	if direction == "UP" || direction == "NORT" {
		for i := 1; i <= step; i++ {
			if maps[x][y+uint8(1)] == "#" {
				fmt.Println("Path Blocked")
				break
			} else {
				y += uint8(1)
				current = [][2]uint8{
					{x, y},
				}
			}
		}
	} else if direction == "RIGHT" || direction == "EAST" {
		for i := 1; i <= step; i++ {
			fmt.Println(current[0])
			if maps[x+uint8(1)][y] == "#" {
				fmt.Println(maps[x+uint8(1)][y])
				fmt.Println(maps[3][1])
				fmt.Println(maps[3][2])
				fmt.Println(maps[3][3])
				fmt.Println("Path Blocked")
				break
			} else {
				x += uint8(1)
				current = [][2]uint8{
					{x, y},
				}
			}
		}
	} else if direction == "DOWN" || direction == "SOUTH" {
		for i := 1; i <= step; i++ {
			if maps[x][y-uint8(1)] == "#" {
				fmt.Println("Path Blocked")
				break
			} else {
				y -= uint8(1)
				current = [][2]uint8{
					{x, y},
				}
			}
		}
	}
}

func main() {
	drawMaps()

	fmt.Println("Start Direction Path")
	fmt.Println("---------------------")

	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		text := scanner.Text()
		var walk = strings.Fields(text)
		i, err := strconv.Atoi(walk[1])
		if err != nil {
			fmt.Println("Step Not Valid")
		}
		setCurrent(strings.ToUpper(walk[0]), i)
		drawMaps()
		// fmt.Println(scanner.Text())
	}
}
